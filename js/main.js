$(function(){
    var standaardCmd = "sudo chmod",
        chmodWaarde = "000",
        bestandsnaam = "filename.txt",
        commandVeld = $('#chmodcommandtocopy');


   // Reset bestandsnaam inputveld ook
   // Bij kiezen van bestandslocatie, moeten twee velden geupdate worden
   $('#filepath').val("").on("change paste keyup", function(e){
      var path = $('#cmdfilepath');
      bestandsnaam = $(this).val();
      // Input veld
      commandVeld.val(standaardCmd+" "+chmodWaarde+" "+bestandsnaam);
      
      path.addClass("cmdfilepathEdit");
      path.text($(this).val());

      if (e.type == "change")
         path.removeClass("cmdfilepathEdit");
   });

   // En ja, value ook resetten bij herladen
   commandVeld.val(standaardCmd+" "+chmodWaarde+" "+bestandsnaam).click(function(){$(this).select()});

   // Reset checkboxes ook
   $('#chmodtbl input').prop("checked", false).change(function(){
      var name = $(this).attr("name"),
          value = parseInt($('#cmdchmodval').text()),
          isChecked = $(this).is(":checked");


      switch (name.substring(0, 1))
      {
         case 'u':
            switch (name)
            {
               case 'uread':
                  value += isChecked ? 400 : -400;
               break;

               case 'uwrite':
                  value += isChecked ? 200 : -200;
               break;

               case 'uexec':
                  value += isChecked ? 100 : -100;
               break;
            }
         break;

         case 'g':
            switch (name)
            {
               case 'gread':
                  value += isChecked ? 40 : -40;
               break;

               case 'gwrite':
                  value += isChecked ? 20 : -20;
               break;

               case 'gexec':
                  value += isChecked ? 10 : -10;
               break;
            }
         break;

         case 'o':
            switch (name)
            {
               case 'oread':
                  value += isChecked ? 4 : -4;
               break;

               case 'owrite':
                  value += isChecked ? 2 : -2;
               break;

               case 'oexec':
                  value += isChecked ? 1 : -1;
               break;
            }
         break;
      }
      
      // Prefix met juiste aantal nullen
      if (value < 10)
         value = "00" + value;
      else if (value < 100 )
         value = "0" + value;

      $('#cmdchmodval').text(value);
      chmodWaarde = value;
      commandVeld.val(standaardCmd+" "+chmodWaarde+" "+bestandsnaam);
   });
   
   // Kopiëren naar klembord functionaliteit
   $('#chmodcommandcopybtn').click(function(){
      commandVeld.select();
      document.execCommand("copy");
   });
})